package x2y

import (
	"reflect"
	"unsafe"
)

func B2S(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func S2B(s string) []byte {
	sh := *(*reflect.StringHeader)(unsafe.Pointer(&s))
	return *(*[]byte)(unsafe.Pointer(&reflect.SliceHeader{
		Len:  sh.Len,
		Cap:  sh.Len,
		Data: sh.Data,
	}))
}
